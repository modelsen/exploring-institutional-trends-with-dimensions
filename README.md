# Exploring institutional trends with Dimensions

This repository contains notebooks to reproduce datasets and figures of the publication

> Exploring institutional trends with Dimensions:
> A comparative bibliometricanalysis of the production of the Max PlanckSociety, 1966-2004

## Running the notebooks

To rerun the notebooks, create a virtual environment with the requirements.
~~~ bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
~~~

The notebooks can then be used with JupyterLab
~~~ bash
jupyter lab
~~~

### Querying the data

For rerunning the queries, users will need a subscription with the Google BigQuery service for Dimensions Analytics. A tutorial is available at [this url](https://bigquery-lab.dimensions.ai/tutorials/01-connection/).

### Rerunning the analysis

To support an explorative phase for the researchers, drop down menues where used
to allow interactive graph creation. Once data was selected for publication,
the selected graphs where exported as static images.

To rerun the full interactive notebook, consider using a local installation like
e.g. Anaconda.
